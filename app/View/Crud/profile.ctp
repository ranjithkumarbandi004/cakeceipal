


<div class="container">
        <div class="row">
        
        <div class="col-4 shadow">
              <div class="card">
                <div class="card-body">
                  <div class="d-flex flex-column align-items-center text-center">
                  
                   <!-- imge -->

                    <?php

                          if(strlen($image)>0){
                            echo '<label for="img">'.$this->Html->image($image,['width'=>'200px','height'=>'200px'])."</label>";
                          }

                    ?>
                          <tr>
                          <td>
                          <?php  echo $this->Form->create('upload',['type'=>'file']);  ?>
                          </td>
                          <td>
                          <?php echo $this->Form->input('image',['type'=>'file','id'=>'img']);     ?>
                          </td>

                          <td>
                          <?php  echo $this->Form->button('upload',['type'=>'submit','class'=>'btn btn-primary']);   ?>
                          </td>

                          <td>
                          <?php echo $this->Form->end(); 
                          
                          
                          


                          //  $this->Flash->render('myerr');

                          //  echo $this->element("mycont");


                          // pr($this->params->query['id'])
                          
                          
                          ?>

                          
                          </td>

                          </tr>

                          <!-- $this->extend('dash'); -->
                          

                     

                    <div class="mt-3">
                      
                    </div>
                  </div>
                </div>
              </div>
              
            </div>


        <div class="col-6 shadow">
              <div class="card mb-3">
                <div class="card-body">
                <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">ID</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                    <h5><?php echo $uid; ?></h5>
                    </div>
                </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Full Name</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                    <h5><?php echo $name; ?></h5>
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Email</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                    <h5><?php  echo $email; ?></h5>
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Mobile</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                    <h5><?php  echo $mobile; ?></h5>
                    </div>


                  </div>
                  <hr>
                  <tr class="row">
                    <td class="col-3">
                      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong">EDIT</button>

                    </td>
                    <td class="col-3 text-secondary">

                        <!-- <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModalLong2">DELETE </button> -->

                      <?php  echo $this->Html->link('DELETE',['controller'=>'Crud','action'=>'delete'],['class'=>'btn btn-danger']);                                ?>


                    </td>
                    <td class="col-3 text-secondary">

                        <!-- <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#exampleModalLong3">Logout </button> -->
                        <?php  echo $this->Html->link('LOGOUT',['controller'=>'Crud','action'=>'logout'],['class'=>'btn btn-warning']);   ?>

                      <!-- <a href="delete">DELETE</a> -->


                      
                    </td>
                 
                        </tr>
            </div>
        </div>



    <!-- model for edit  -->
        <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">EDIT Form</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">

              <?php
                      echo $this->Form->create('edit',['controller'=>'Rbc','url'=>'edit']);

              ?>

                <div class="row">
                  <div class="col-6 col-md-12">
                    <?php
                      echo $this->Form->input('name',['value'=>$name]);
                    ?>

                  </div>
                  <div class="col-6 col-md-12">
                  <?php
                      echo $this->Form->input('mobile',['value'=>$mobile]);
                    ?>
                  </div>
                  <div class="col-6 col-md-12">
                  <?php
                      echo $this->Form->input('email',['value'=>$email]);

                    ?>
                  </div>
                </div>
            

            </div>
            <div class="modal-footer">

              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <?php
                 
                  echo $this->Form->end('update',['class'=>'btn btn-primary']);
              ?>

            </div>
            </form>
          </div>
        </div>
      </div>

    </div>

  <!-- edit model end  -->



<!-- delete confirm model  -->
    <div class="modal fade" id="exampleModalLong2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">You want to delete Your account</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              </button>
            </div>

            <?php
                    echo $this->Form->create('dele',['controller'=>'Rbc','url'=>'delete']);
              ?>
           
            <div class="modal-footer">

              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <?php

                  echo $this->Form->end('confirm',['class'=>'btn btn-warning']);



                 
              ?>
              
            </div>
            </form>
          </div>
        </div>
      </div>

    </div>
<!-- delete model end -->



<!-- logout confirm model  -->
<div class="modal fade" id="exampleModalLong3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">You want to logout Your account</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              </button>
            </div>

            <?php
                        
                echo $this->Form->create('dele',['controller'=>'Rbc','url'=>'logout']);

            ?>
           
            <div class="modal-footer">

              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <?php

                  echo $this->Form->end('confirm',['class'=>'btn btn-warning']);



              ?>


              
            </div>
            </form>

                 
            <!-- <php   echo $this->Html->button('click');  ?> -->


         


          </div>
        </div>
      </div>

    </div>
<!-- delete model end -->

<script>

    $(document).ready(function () {


        // $("#pic_load" ).on( "submit", function(e) {
            
        //     e.preventDefault();

        //     var img = $("#img").val();

        //     $.ajax({
                
        //         method: 'POST',
        //         url:"Rbc/dash",
        //         data: {img:img},
        //         success:function(d){
        //             // $("#add").html(d);

        //             // alert(d);
        //             console.log(d);
        //             // if(d == 1){

        //             //     window.location = 'Rbc/dash';

        //             // }else{

        //             //     alert('you have enter wrong credentials');
        //             // }
        //         }
                
        //     });	


        //     // console.log(img);



        // });

        // $("#pic_load").validate({


        //     rules: {
            
        //     'image': {
        //         required: true

        //     }

        //     },

        //     messages: {

        //     'image': "Please select image "

        //     }


        // });


    //     $(".test").click(function(){


    //       $(location).attr("href", "reguser");
                 


    //     })







    });




</script>


    
</body>
</html>